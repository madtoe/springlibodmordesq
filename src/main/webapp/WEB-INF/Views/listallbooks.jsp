<%@ page import="pl.sda.bookstore.Beans.Bookk" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: marekdybusc
  Date: 09.02.2018
  Time: 18:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>BookList</title>
</head>
<body>
<h1>BookList</h1>

<%
    List<Bookk> thelist = (List<Bookk>) request.getAttribute("books");

    for (Bookk b : thelist) {
        out.print("<p>" + b.getAuthor().getName() + ": &nbsp;" + b.getTitle() + "</p>");
    }

%>
</body>
</html>
