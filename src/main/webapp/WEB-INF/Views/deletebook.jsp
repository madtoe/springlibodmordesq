<%--
  Created by IntelliJ IDEA.
  User: marekdybusc
  Date: 09.02.2018
  Time: 18:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Book Deleted</title>
</head>
<body>
<h1>
    Deleted book:
</h1>

<h3>${title} &nbsp; by &nbsp; ${author}</h3>

</body>
</html>
