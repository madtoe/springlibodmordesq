package pl.sda.bookstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.sda.bookstore.Beans.Authorr;
import pl.sda.bookstore.Beans.Bookk;
import pl.sda.bookstore.Beans.IAuthorDAO;
import pl.sda.bookstore.Beans.IBookDAO;

import javax.transaction.Transactional;
import java.util.List;

@Controller
public class AuthorCrudController {

    @Autowired
    IAuthorDAO authorDAO;

    @GetMapping("/author/add/{name:[A-Za-z0-9]+}")
    public String addAuthor(@PathVariable("name") String name, ModelMap model){
        Authorr author = new Authorr();
        author.setName(name);
        authorDAO.saveAuthor(author);
        model.addAttribute("name", name);
        model.addAttribute("id", author.getId());
        return "addauthor";
    }

    @GetMapping("/author/view/{id:[\\d]+}")
    @Transactional
    public String viewAuthor(@PathVariable("id") long id, ModelMap modelMap){

        Authorr author = authorDAO.getAuthor(id);
        modelMap.addAttribute("name", author.getName());
        modelMap.addAttribute("id", author.getId());
        return "viewingauthor";
    }

    @GetMapping("/author/list")
    @Transactional
    public String viewAuthorList(ModelMap modelMap) {
        List<Authorr> allAuthors = authorDAO.getAllAuthors();
        modelMap.addAttribute("authors", allAuthors);
        return "listallauthors";
    }

    @GetMapping("/author/delete/{id:[0-9]+}")
    @Transactional
    public String deleteAuthor(@PathVariable("id") long id, ModelMap modelMap) {
        Authorr author = authorDAO.getAuthor(id);
        modelMap.addAttribute("name", author.getName());
        modelMap.addAttribute("id", author.getId());
        authorDAO.deleteAuthor(id);
        return "deleteauthor";
    }
}
