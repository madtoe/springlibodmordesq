package pl.sda.bookstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import pl.sda.bookstore.Beans.IUserDAO;
import pl.sda.bookstore.Beans.Userr;

import java.util.ArrayList;
import java.util.List;

public class DBAuthenticationManager implements AuthenticationManager {

    @Autowired
    IUserDAO userDAO;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String login = authentication.getName();
        String credentials = (String) authentication.getCredentials();
        Userr user = userDAO.getUser(login, credentials);
        if(null != user) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
            return new UsernamePasswordAuthenticationToken(login, credentials, grantedAuths);
        }
        return authentication;
    }
}
