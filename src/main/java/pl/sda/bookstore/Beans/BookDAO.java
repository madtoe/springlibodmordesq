package pl.sda.bookstore.Beans;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;

@Repository
public class BookDAO implements IBookDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    @Override
    public void saveBook(Bookk bookk) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(bookk);
    }

    @Transactional
    @Override
    public Bookk getBook(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.load(Bookk.class, id);
    }

    @Transactional
    @Override
    public List<Bookk> getAllBooks() {
        Session session = sessionFactory.getCurrentSession();
        LinkedList<Bookk> allBookks = new LinkedList<>();
        List resultList = session.createQuery("FROM Bookk").getResultList();
        for (Object o : resultList) {
            if (o instanceof Bookk) {
                allBookks.add((Bookk) o);
            }
        }
        return allBookks;
    }

    @Transactional

    @Override
    public Bookk deleteBook(long id) {
        Session session = sessionFactory.getCurrentSession();
        Bookk book = getBook(id);
        session.delete(book);
        return book;
    }
}
