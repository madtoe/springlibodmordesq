package pl.sda.bookstore.Beans;

import javax.transaction.Transactional;
import java.util.List;

public interface IBookDAO {

    @Transactional
    void saveBook(Bookk bookk);

    @Transactional
    Bookk getBook(long id);

    @Transactional
    List<Bookk> getAllBooks();

    @Transactional
    Bookk deleteBook(long id);
}
