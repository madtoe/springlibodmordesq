package pl.sda.bookstore.Beans;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;

@Repository
public class AuthorDAO implements IAuthorDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    @Override
    public void saveAuthor(Authorr authorr) {
        getSession().saveOrUpdate(authorr);
    }

    @Transactional
    @Override
    public Authorr getAuthor(long id) {
        return getSession().load(Authorr.class, id);
    }

    @Transactional
    @Override
    public List<Authorr> getAllAuthors() {
        LinkedList<Authorr> allAuthors = new LinkedList<>();
        List resultList = getSession().createQuery("FROM Authorr").getResultList();
        for (Object o : resultList) {
            if (o instanceof Authorr) {
                allAuthors.add((Authorr) o);
            }
        }
        return allAuthors;
    }

    @Transactional
    @Override
    public Authorr deleteAuthor(long id) {
        Authorr author = getAuthor(id);
        deleteAuthorsBooks(author);
        getSession().delete(author);
        return author;
    }

    @Transactional
    public void deleteAuthorsBooks(Authorr author) {
        Query query = getSession().createQuery("DELETE Bookk WHERE author = :author").setParameter("author", author);
        query.executeUpdate();
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
