package pl.sda.bookstore.Beans;

import javax.transaction.Transactional;
import java.util.List;

public interface IAuthorDAO {

    @Transactional
    void saveAuthor(Authorr author);

    @Transactional
    Authorr getAuthor(long id);

    @Transactional
    List<Authorr> getAllAuthors();

    @Transactional
    Authorr deleteAuthor(long id);
}
