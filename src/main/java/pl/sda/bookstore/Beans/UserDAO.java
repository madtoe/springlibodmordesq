package pl.sda.bookstore.Beans;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserDAO implements IUserDAO{

    @Autowired
    SessionFactory sessionFactory;


    @Transactional
    @Override
    public Userr getUser(String login, String password){
        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.createQuery("FROM Userr WHERE login = :login AND password = :password")
                .setParameter("login", login)
                .setParameter("password", password);
        List resultList = query.getResultList();
        if (resultList.size() > 0) {
            return (Userr) resultList.get(0);
        }
        return null;
    }

    @Transactional
    @Override
    public void addUser(Userr user) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.persist(user);
    }
}
