package pl.sda.bookstore.Beans;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Scope("prototype")
public class Bookk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Authorr author;

    @Column
    private String title;



    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Authorr getAuthor() {
        return author;
    }

    public void setAuthor(Authorr author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
