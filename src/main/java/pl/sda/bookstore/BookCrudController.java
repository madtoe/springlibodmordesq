package pl.sda.bookstore;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.sda.bookstore.Beans.*;

import javax.transaction.Transactional;
import java.util.List;

@Controller
public class BookCrudController {

    @Autowired
    IBookDAO bookDAO;

    @Autowired
    IAuthorDAO authorDAO;

    @Autowired
    IUserDAO userDAO;

    @GetMapping("/book/add/{title:[A-Za-z0-9 ]+}/{authorID:[0-9]+}")
    public String addBook(@PathVariable("title") String title, @PathVariable("authorID") long authorID, ModelMap model) {
        Bookk bookk = new Bookk();
        bookk.setAuthor(authorDAO.getAuthor(authorID));
        bookk.setTitle(title);
        bookDAO.saveBook(bookk);
        return "addbook";

    }

    @GetMapping("/book/view/{id:[\\d]+}")
    @Transactional
    public String viewBook(@PathVariable("id") String id, ModelMap modelMap) {

        Bookk book = bookDAO.getBook(Long.parseLong(id));
        modelMap.addAttribute("title", book.getTitle());
        modelMap.addAttribute("author", book.getAuthor().getName());
        return "viewingbook";
    }

    @GetMapping("/book/list")
    @Transactional
    public String viewBookList(ModelMap modelMap) {
        List<Bookk> allBooks = bookDAO.getAllBooks();
        modelMap.addAttribute("books", allBooks);
        return "listallbooks";
    }

    @GetMapping("/book/delete/{id:[0-9]+}")
    @Transactional
    public String deleteBook(@PathVariable("id") long id, ModelMap modelMap) {
        Bookk book = bookDAO.getBook(id);
        modelMap.addAttribute("author", book.getAuthor().getName());
        modelMap.addAttribute("title", book.getTitle());
        bookDAO.deleteBook(id);
        return "deletebook";
    }

    @GetMapping("/user/add/{login:[A-Za-z0-9 ]+}/{password:[A-Za-z0-9 ]+}")
    @Transactional
    public String addUser(@PathVariable("login") String login, @PathVariable("password") String password, ModelMap model){
        Userr user = new Userr();
        user.setLogin(login);
        user.setPassword(password);
        userDAO.addUser(user);
        return "addbook";
    }

}

